﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Biblioteka.Models;
using Biblioteka.Connection;
using Biblioteka.ViewModel;
using Biblioteka.AddModel;
using Biblioteka.InputModel;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace Biblioteka.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private APIConnection _apiConnection;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            _apiConnection = new APIConnection();
        }

        public IActionResult Index(ErrorViewModel errorViewModel)
        {
            string information;
            bool success;

            BooksViewModel booksViewModel = new BooksViewModel()
            {
                Books = _apiConnection.GetListBooks(GetToken(), out success, out information),
                Type = GetTypeList(),
                Error = errorViewModel.RequestId
            };

            if (success) return View(booksViewModel);
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information + " " + booksViewModel.Error });
            }
        }

        [HttpPost]
        public IActionResult Index(BooksViewModel booksViewModel)
        {
            string information;
            bool success;

            if (booksViewModel.Result != null && booksViewModel.Result.Trim().Length > 0)
            {
                if (booksViewModel.TypeResult == "Tytuł") booksViewModel.Books = _apiConnection.FindBookByTitle(booksViewModel.Result, GetToken(), out success, out information);
                else if (booksViewModel.TypeResult == "Rok Wydania") booksViewModel.Books = _apiConnection.FindBookByDate(booksViewModel.Result, GetToken(), out success, out information);
                else booksViewModel.Books = _apiConnection.FindBookByAuthor(booksViewModel.Result, GetToken(), out success, out information);
            }
            else
            {
                booksViewModel.Books = _apiConnection.GetListBooks(GetToken(), out success, out information);
            }

            booksViewModel.Type = GetTypeList();

            if (success) return View(booksViewModel);
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
            }
        }

        public IActionResult AddBook()
        {
            AddBookInputModel inputModel = new AddBookInputModel();

            return View(inputModel);
        }

        [HttpPost]
        public IActionResult AddBook(AddBookInputModel inputModel)
        {
            bool success;
            string information;

            if (ModelState.IsValid)
            {
                if (inputModel.publishYear < 1000 || inputModel.publishYear > int.Parse(DateTime.Now.Year.ToString()))
                {
                    ModelState.AddModelError("publishYear", "Podany rok jest nieprawidłowy");
                }
                else if (inputModel.numberOfAllBooks < 1)
                {
                    ModelState.AddModelError("numberOfAllBooks", "Podana liczba jest nieprawidłowa");
                }
                else
                {
                    AddBook addBook = new AddBook()
                    {
                        title = inputModel.title,
                        publishYear = (int)inputModel.publishYear,
                        numberOfAllBooks = (int)inputModel.numberOfAllBooks,
                        authors = new List<AddAuthor>()
                    };

                    AddAuthor addAuthor = new AddAuthor()
                    {
                        firstName = inputModel.firstName,
                        secondName = inputModel.secondName
                    };

                    addBook.authors.Add(addAuthor);

                    _apiConnection.AddBook(addBook, GetToken(), out success, out information);

                    if (success) return RedirectToAction("Index");
                    else if (information == "Książka o tym tytule już istnieje")
                    {
                        ModelState.AddModelError("title", information);
                        return View(inputModel);
                    }
                    else
                    {
                        HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                        return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
                    }
                }
            }

            return View(inputModel);
        }

        public IActionResult ShowBook(int id)
        {
            bool success;
            string information;

            Book book = _apiConnection.FindBook(id, GetToken(), out success, out information);

            if (success)
            {
                ShowBookViewModel showBookViewModel = new ShowBookViewModel()
                {
                    firstName = book.authors[0].firstName,
                    secondName = book.authors[0].secondName,
                    publishYear = book.publishYear,
                    title = book.title,
                    numberOfAllBooks = book.numberOfAllBooks,
                    numberOfAvailableBooks = book.numberOfAvailableBooks,
                    id = id
                };

                return View(showBookViewModel);
            }
            else if (information == "Ten element już nie istnieje w bazie danych") return RedirectToAction("Index", new ErrorViewModel() { RequestId = information });
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
            }
        }

        public IActionResult UpdateBook(int id)
        {
            bool success;
            string information;

            Book book = _apiConnection.FindBook(id, GetToken(), out success, out information);

            if (success)
            {
                UpdateBookInputModel updateBookInputModel = new UpdateBookInputModel()
                {
                    id = book.id,
                    firstName = book.authors[0].firstName,
                    secondName = book.authors[0].secondName,
                    publishYear = book.publishYear,
                    title = book.title,
                    numberOfAllBooks = book.numberOfAllBooks,
                    numberOfUnavailableBooks = book.numberOfAllBooks - book.numberOfAvailableBooks
                };

                return View(updateBookInputModel);
            }
            else if (information == "Ten element już nie istnieje w bazie danych") return RedirectToAction("Index", new ErrorViewModel() { RequestId = information });
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
            }
        }

        [HttpPost]
        public IActionResult UpdateBook(UpdateBookInputModel updateBookInputModel)
        {
            if (ModelState.IsValid)
            {
                if (updateBookInputModel.publishYear < 1000 || updateBookInputModel.publishYear > int.Parse(DateTime.Now.Year.ToString()))
                {
                    ModelState.AddModelError("publishYear", "Podany rok jest nieprawidłowy");
                }
                else if (updateBookInputModel.numberOfAllBooks < updateBookInputModel.numberOfUnavailableBooks)
                {
                    ModelState.AddModelError("numberOfAllBooks", "Podana liczba jest mniejsza niż liczba wypożyczonych książek - " + updateBookInputModel.numberOfUnavailableBooks);
                }
                else
                {
                    bool success;
                    string information;

                    AddBook addBook = new AddBook()
                    {
                        title = updateBookInputModel.title,
                        publishYear = (int)updateBookInputModel.publishYear,
                        authors = new List<AddAuthor>(),
                        numberOfAllBooks = (int)updateBookInputModel.numberOfAllBooks
                    };

                    AddAuthor addAuthor = new AddAuthor()
                    {
                        firstName = updateBookInputModel.firstName,
                        secondName = updateBookInputModel.secondName
                    };

                    addBook.authors.Add(addAuthor);

                    _apiConnection.UpdateBook(updateBookInputModel.id, addBook, GetToken(), out success, out information);

                    if (success) return RedirectToAction("Index");
                    else if (information == "Książka o tym tytule już istnieje")
                    {
                        ModelState.AddModelError("title", information);
                        return View(updateBookInputModel);
                    }
                    else if (information == "Ten element już nie istnieje w bazie danych") return RedirectToAction("Index", new ErrorViewModel() { RequestId = information });
                    else
                    {
                        HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                        return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
                    }
                }
            }

            return View(updateBookInputModel);
        }

        public IActionResult RemoveBook(int id)
        {
            bool success;
            string information;

            _apiConnection.RemoveBook(id, GetToken(), out success, out information);

            if (success) return RedirectToAction("Index");
            else if (information == "Ten element już nie istnieje w bazie danych" || information == "Operacja nie powiodła się - nie wszystkie egzemplarze książki zostały oddane") return RedirectToAction("Index", new ErrorViewModel() { RequestId = information });
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
            }
        }

        public IActionResult CheckBookOutList(ErrorViewModel errorViewModel)
        {
            bool success;
            string information;

            CheckBookOutListViewModel checkBookOutListViewModel = new CheckBookOutListViewModel()
            {
                Rents = _apiConnection.GetRentListById(GetId(), GetToken(), out success, out information),
                Error = errorViewModel.RequestId
            };

            if (success) return View(checkBookOutListViewModel);
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
            }
        }

        public IActionResult CheckBookOut(int id)
        {
            AddRent addRent = new AddRent()
            {
                bookId = id,
                userId = GetId()
            };

            bool success;
            string information;

            _apiConnection.PostNewRent(addRent, GetToken(), out success, out information);

            if (success) return RedirectToAction("Index", new ErrorViewModel() { RequestId = "Wypożyczenie zostało zrealizowane" });
            else if (information == "Wszystkie są wypożyczone") return RedirectToAction("Index", new ErrorViewModel() { RequestId = information });
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
            }
        }

        public IActionResult ReturnBook(int id)
        {
            bool success;
            string information;

            _apiConnection.ReturnRentedbook(id, GetToken(), out success, out information);

            if (success)
            {
                return RedirectToAction("CheckBookOutList", new ErrorViewModel() { RequestId = information });
            }
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private string GetToken()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return identity.FindFirst("Token").Value;
        }

        private int GetId()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return int.Parse(identity.FindFirst("Id").Value);
        }

        private List<string> GetTypeList()
        {
            List<string> result = new List<String>();

            result.Add("Tytuł");
            result.Add("Rok Wydania");
            result.Add("Autor");

            return result;
        }
    }
}
