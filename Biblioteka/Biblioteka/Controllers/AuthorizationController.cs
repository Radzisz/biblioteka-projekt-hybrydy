﻿using Biblioteka.Connection;
using Biblioteka.InputModel;
using Biblioteka.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Biblioteka.Controllers
{
    public class AuthorizationController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private APIConnection _apiConnection;

        public AuthorizationController(ILogger<HomeController> logger)
        {
            _logger = logger;
            _apiConnection = new APIConnection();
        }

        public IActionResult LogIn(ErrorViewModel errorViewModel)
        {
            LogInInputModel logInInputModel = new LogInInputModel();

            logInInputModel.Error = errorViewModel.RequestId;

            return View(logInInputModel);
        }

        [HttpPost]
        public async Task<IActionResult> LogInAsync(LogInInputModel logInInputModel)
        {
            bool success;
            string information;

            User user = new User()
            {
                username = logInInputModel.username,
                password = logInInputModel.password
            };

            Token token = _apiConnection.Authenticate(user, out success, out information);

            if (success)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.username),
                    new Claim("Token", token.jwt),
                    new Claim("Id", token.id.ToString()),
                    new Claim(ClaimTypes.Role, token.role)
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var authProperties = new AuthenticationProperties();

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), authProperties);
                
                return RedirectToAction("Index", "Home");
            }
            else
            {
                logInInputModel.Error = information;
            }

            return View(logInInputModel);
        }

        public IActionResult LogOut()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = "Wylogowano pomyślnie" });
        }


    }
}