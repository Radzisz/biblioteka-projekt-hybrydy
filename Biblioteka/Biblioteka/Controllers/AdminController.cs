﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Biblioteka.AddModel;
using Biblioteka.Connection;
using Biblioteka.InputModel;
using Biblioteka.Models;
using Biblioteka.Security.Roles;
using Biblioteka.ViewModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Biblioteka.Controllers
{
    [Authorize(Roles = "ROLE_ADMIN")]
    public class AdminController : Controller
    {
        private readonly ILogger<AdminController> _logger;
        private APIConnection _apiConnection;

        public AdminController(ILogger<AdminController> logger)
        {
            _logger = logger;
            _apiConnection = new APIConnection();
        }

        public IActionResult Index(ErrorViewModel errorViewModel)
        {
            string information;
            bool success;

            UserViewModel usersViewModel = new UserViewModel()
            {
                Users = _apiConnection.GetListUsers(GetToken(), out success, out information),
                Error = errorViewModel.RequestId
            };

            if (success) return View(usersViewModel);
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information + " " + usersViewModel.Error });
            }
        }

        [HttpPost]
        public IActionResult Index(UserViewModel usersViewModel)
        {
            string information;
            bool success;

            if (usersViewModel.Result != null && usersViewModel.Result.Trim().Length > 0)
            {
                usersViewModel.Users = new List<User>();
                usersViewModel.Users.Add(_apiConnection.FindUserByName(usersViewModel.Result, GetToken(), out success, out information));
            }
            else
            {
                usersViewModel.Users = _apiConnection.GetListUsers(GetToken(), out success, out information);
            }

            if (success) return View(usersViewModel);
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
            }
        }

        public IActionResult RemoveUser(int id)
        {
            bool success;
            string information;

            _apiConnection.RemoveUser(id, GetToken(), out success, out information);

            if (success) return RedirectToAction("Index");
            else if (information == "Ten element już nie istnieje w bazie danych") return RedirectToAction("Index", new ErrorViewModel() { RequestId = information });
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
            }
        }
        public IActionResult AddUser()
        {
            AddUserInputModel inputModel = new AddUserInputModel();

            List<string> roles = new List<string>();
            roles.Add("User");
            roles.Add("Admin");

            ViewBag.Roles = roles;

            return View(inputModel);
        }

        [HttpPost]
        public IActionResult AddUser(AddUserInputModel inputModel)
        {
            bool success;
            string information;
            string _role = "ROLE_USER";

            if (inputModel.roles == "Admin")
            {
                _role = "ROLE_ADMIN";
            }

            List<string> roles = new List<string>();
            roles.Add("User");
            roles.Add("Admin");

            ViewBag.Roles = roles;

            if (ModelState.IsValid)
            {
                AddUser addUser = new AddUser()
                {
                    username = inputModel.username,
                    password = GeneratePassword(10),
                    roles = _role
                };

                _apiConnection.AddUser(addUser, GetToken(), out success, out information);

                if (success) return RedirectToAction("Index");
                else if (information == "Użytkownik o danej nazwie już istnieje")
                {
                    ModelState.AddModelError("username", information);
                    return View(inputModel);
                }
                else
                {
                    HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                    return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
                }
            }

            return View(inputModel);
        }

        public IActionResult RentList(ErrorViewModel errorViewModel)
        {
            bool success;
            string information;

            CheckBookOutListViewModel checkBookOutListViewModel = new CheckBookOutListViewModel()
            {
                Rents = _apiConnection.GetRentList(GetToken(), out success, out information),
                Error = errorViewModel.RequestId
            };

            if (success) return View(checkBookOutListViewModel);
            else
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("LogIn", "Authorization", new ErrorViewModel() { RequestId = information });
            }
        }
        private string GetToken()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return identity.FindFirst("Token").Value;
        }

        private int GetId()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return int.Parse(identity.FindFirst("Id").Value);
        }

        private string GeneratePassword(int PasswordLength)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }
    }
}