﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.AddModel
{
    public class AddAuthor
    {
        public string firstName { get; set; }
        public string secondName { get; set; }
    }
}
