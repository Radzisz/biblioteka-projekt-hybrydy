﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.AddModel
{
    public class AddBook
    {
        public string title { get; set; }
        public List<AddAuthor> authors { get; set; }
        public int publishYear { get; set; }
        public int numberOfAllBooks { get; set; }
    }
}
