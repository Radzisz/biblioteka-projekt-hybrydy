﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.AddModel
{
    public class AddRent
    {
        public int bookId { get; set; }
        public int userId { get; set; }
    }
}
