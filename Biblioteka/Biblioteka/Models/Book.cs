﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.Models
{
    public class Book
    {
        public int id { get; set; }
        public string title { get; set; }
        public List<Author> authors { get; set; }
        public int publishYear { get; set; }
        public int numberOfAllBooks { get; set; }
        public int numberOfAvailableBooks { get; set; }
    }
}
