﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.Models
{
    public class Token
    {
        public string jwt { get; set; }
        public int id { get; set; }
        public string role { get; set; }
    }
}
