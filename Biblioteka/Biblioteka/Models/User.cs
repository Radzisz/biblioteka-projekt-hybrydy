﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.Models
{
    public class User
    {
        public int id { get; set; }
        [Required]
        public string username { get; set; }
        public string password { get; set; }
        public string roles { get; set; }
    }
}
