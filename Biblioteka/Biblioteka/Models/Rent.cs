﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.Models
{
    public class Rent
    {
        public int id { get; set; }
        public int bookid { get; set; }
        public int userid { get; set; }
        public string rentDateTime { get; set;}
        public bool active { get; set; }
    }
}
