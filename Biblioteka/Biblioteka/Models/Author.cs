﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.Models
{
    public class Author
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
    }
}
