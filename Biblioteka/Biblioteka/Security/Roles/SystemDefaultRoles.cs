﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.Security.Roles
{
    public class SystemDefaultRoles
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}
