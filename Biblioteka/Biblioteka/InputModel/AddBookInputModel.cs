﻿using System;
using System.ComponentModel.DataAnnotations;


namespace Biblioteka.InputModel
{
    public class AddBookInputModel
    {
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Pole tytułu jest puste")]
        public string title { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Pole imienia jest puste")]
        public string firstName { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Pole nazwiska jest puste")]
        public string secondName { get; set; }

        [Required(ErrorMessage = "Pole rok wydania jest puste")]
        public int? publishYear { get; set; }

        [Required(ErrorMessage = "Pole liczby książek jest puste")]
        public int? numberOfAllBooks { get; set; }
    }
}
