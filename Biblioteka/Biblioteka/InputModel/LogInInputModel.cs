﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.InputModel
{
    public class LogInInputModel
    {
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Pole nazwy użytkownika jest puste")]
        public string username { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Pole hasła jest puste")]
        public string password { get; set; }

        public string Error { get; set; }
    }
}
