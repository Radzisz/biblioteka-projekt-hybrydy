﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.InputModel
{
    public class AddUserInputModel
    {
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Nazwa jest wymagana")]
        public string username { get; set; }
        public string password { get; set; }
        public string roles { get; set; }
    }
}
