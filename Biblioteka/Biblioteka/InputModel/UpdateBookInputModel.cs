﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.InputModel
{
    public class UpdateBookInputModel
    {
        public int id { get; set; }

        public int numberOfUnavailableBooks { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Pole tytułu jest puste")]
        public string title { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Pole imienia jest puste")]
        public string firstName { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Pole nazwiska jest puste")]
        public string secondName { get; set; }

        [Required(ErrorMessage = "Pole roku wydania jest puste")]
        public int? publishYear { get; set; }

        [Required(ErrorMessage = "Pole liczby książek jest puste")]
        public int? numberOfAllBooks { get; set; }
    }
}
