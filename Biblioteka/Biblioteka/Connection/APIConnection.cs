﻿using Biblioteka.AddModel;
using Biblioteka.Models;
using Nancy.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka.Connection
{
    public class APIConnection
    {
        private string URL;
        private JavaScriptSerializer javaScriptSerializer;

        public APIConnection()
        {
            URL = "https://hybrydy-200611184738.azurewebsites.net/";
            javaScriptSerializer = new JavaScriptSerializer();
        }

        public Token Authenticate(User user, out bool success, out string information)
        {
            string json = javaScriptSerializer.Serialize(user);

            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(URL + "authenticate"),
                Headers = {
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },

                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };

            Token token = new Token();

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        token = javaScriptSerializer.Deserialize<Token>(response.Content.ReadAsStringAsync().Result);
                        success = true;
                        information = "Operacja się powiodła";
                    }
                    else
                    {
                        success = false;

                        if (response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            information = "Podano zły login lub hasło, prosze spróbować jeszcze raz";
                        }
                        else
                        {
                            information = "Błąd przy łączeniu się z API";
                        }
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
            }

            return token;
        }

        public List<Book> GetListBooks(string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(URL + "book"),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },
            };

            List<Book> books = new List<Book>();

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            books = javaScriptSerializer.Deserialize<List<Book>>(result);
                        }

                        success = true;
                        information = "";

                        return books;
                    }
                    else
                    {
                        success = false;

                        if (response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                        }
                        else
                        {
                            information = "Błąd przy łączeniu się z API";
                        }
                    }

                    return books;
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
                return books;
            }     
        }

        public void AddBook(AddBook book, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            string json = javaScriptSerializer.Serialize(book);

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(URL + "book"),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },

                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        success = true;
                        information = "";
                    }
                    else if(response.StatusCode == HttpStatusCode.NotAcceptable)
                    {
                        success = false;
                        information = "Książka o tym tytule już istnieje";
                    }
                    else if (response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        success = false;
                        information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                    }
                    else
                    {
                        success = false;
                        information = "Błąd przy łączeniu się z API";
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
            }
        }

        public void RemoveBook(int id, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri(URL + "book/" + id),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },
            };

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        success = true;
                        information = "";
                    }
                    else if(response.StatusCode == HttpStatusCode.NotAcceptable)
                    {
                        success = false;
                        information = "Operacja nie powiodła się - nie wszystkie egzemplarze książki zostały oddane";
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        success = false;
                        information = "Ten element już nie istnieje w bazie danych";
                    }
                    else if (response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        success = false;
                        information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                    }
                    else
                    {
                        success = false;
                        information = "Błąd przy łączeniu się z API";
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
            }
        }

        public List<Book> FindBookByTitle(string title, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(URL + "book/title?title=" + title),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },
            };

            List<Book> books = new List<Book>();

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            books = javaScriptSerializer.Deserialize<List<Book>>(result);
                        }

                        success = true;
                        information = "";

                        return books;
                    }
                    else
                    {
                        success = false;

                        if (response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                        }
                        else
                        {
                            information = "Błąd przy łączeniu się z API";
                        }

                        return books;
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
                return books;
            }
        }

        public List<Book> FindBookByDate(string date, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(URL + "book/publishYear?publishYear=" + date),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },
            };

            List<Book> books = new List<Book>();

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            books = javaScriptSerializer.Deserialize<List<Book>>(result);
                        }

                        success = true;
                        information = "";
                    }
                    else
                    {
                        success = false;

                        if (response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                        }
                        else
                        {
                            information = "Błąd przy łączeniu się z API";
                        }
                    }

                    return books;
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
                return books;
            }
        }

        public List<Book> FindBookByAuthor(string author, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(URL + "book/author?author=" + author),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },
            };

            List<Book> books = new List<Book>();

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            books = javaScriptSerializer.Deserialize<List<Book>>(result);
                        }

                        success = true;
                        information = "";
                    }
                    else
                    {
                        success = false;

                        if (response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                        }
                        else
                        {
                            information = "Błąd przy łączeniu się z API";
                        }
                    }

                    return books;
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
                return books;
            }
        }

        public Book FindBook(int id, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(URL + "book/" + id),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },
            };

            Book book = new Book();

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;

                        book = javaScriptSerializer.Deserialize<Book>(result);

                        success = true;
                        information = "";

                        return book;
                    }
                    else
                    {
                        success = false;

                        if(response.StatusCode == HttpStatusCode.NotFound)
                        {
                            information = "Ten element już nie istnieje w bazie danych";
                        }
                        else if (response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                        }
                        else
                        {
                            information = "Błąd przy łączeniu się z API";
                        }

                        return book;
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
                return book;
            }
        }

        public void UpdateBook(int id, AddBook book, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            string json = javaScriptSerializer.Serialize(book);

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Put,
                RequestUri = new Uri(URL + "book/" + id),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },

                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        success = true;
                        information = "";
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        success = false;
                        information = "Ten element już nie istnieje w bazie danych";
                    }
                    else if (response.StatusCode == HttpStatusCode.NotAcceptable)
                    {
                        success = false;
                        information = "Książka o tym tytule już istnieje";
                    }
                    else if (response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        success = false;
                        information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                    }
                    else
                    {
                        success = false;
                        information = "Błąd przy łączeniu się z API";
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
            }
        }

        public List<User> GetListUsers(string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(URL + "admin/user"),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },
            };

            List<User> users = new List<User>();

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            users = javaScriptSerializer.Deserialize<List<User>>(result);
                        }

                        success = true;
                        information = "";

                        return users;
                    }
                    else
                    {
                        success = false;

                        if (response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                        }
                        else
                        {
                            information = "Błąd przy łączeniu się z API";
                        }
                    }
                    return users;
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
                return users;
            }
        }

        public User FindUserByName(string username, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(URL + "admin/user/" + username),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },
            };

            User users = new User();

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            users = javaScriptSerializer.Deserialize<User>(result);
                        }

                        success = true;
                        information = "";

                        return users;
                    }
                    else
                    {
                        success = false;

                        if (response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                        }
                        else
                        {
                            information = "Błąd przy łączeniu się z API";
                        }

                        return users;
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
                return users;
            }
        }

        public void RemoveUser(int id, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri(URL + "admin/user/id/" + id),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },
            };

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        success = true;
                        information = "";
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        success = false;
                        information = "Ten element już nie istnieje w bazie danych";
                    }
                    else if (response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        success = false;
                        information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                    }
                    else
                    {
                        success = false;
                        information = "Błąd przy łączeniu się z API";
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
            }
        }
        public void AddUser(AddUser user, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            string json = javaScriptSerializer.Serialize(user);

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(URL + "admin/user"),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },

                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        success = true;
                        information = "";
                    }
                    else if (response.StatusCode == HttpStatusCode.NotAcceptable)
                    {
                        success = false;
                        information = "Użytkownik o danej nazwie już istnieje";
                    }
                    else if (response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        success = false;
                        information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                    }
                    else
                    {
                        success = false;
                        information = "Błąd przy łączeniu się z API";
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
            }
        }

        public void PostNewRent(AddRent addRent, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            string json = javaScriptSerializer.Serialize(addRent);

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(URL + "rent"),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },

                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        success = true;
                        information = "";
                    }
                    else if (response.StatusCode == HttpStatusCode.NotAcceptable)
                    {
                        success = false;
                        information = "Wszystkie są wypożyczone";
                    }
                    else if (response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        success = false;
                        information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                    }
                    else
                    {
                        success = false;
                        information = "Błąd przy łączeniu się z API";
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
            }
        }

        public void ReturnRentedbook(int id, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(URL + "rent/return/" + id),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },
            };

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        success = true;
                        information = "Książka została oddana";
                    }
                    else if (response.StatusCode == HttpStatusCode.NotAcceptable)
                    {
                        success = true;
                        information = "Ksiązka została już wcześniej oddana";
                    }
                    else if (response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        success = false;
                        information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                    }
                    else
                    {
                        success = false;
                        information = "Błąd przy łączeniu się z API";
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
            }
        }

        public List<Rent> GetRentListById(int id, string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(URL + "rent/user/" + id),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                }
            };

            List<Rent> rents = new List<Rent>();

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            rents = javaScriptSerializer.Deserialize<List<Rent>>(result);
                        }

                        success = true;
                        information = "";

                        return rents;
                    }
                    else
                    {
                        success = false;

                        if (response.StatusCode == HttpStatusCode.NotFound)
                        {
                            information = "Użytkownik nie został znaleziony";
                        }
                        else if (response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                        }
                        else
                        {
                            information = "Błąd przy łączeniu się z API";
                        }

                        return rents;
                    }
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
                return rents;
            }
        }

        public List<Rent> GetRentList(string token, out bool success, out string information)
        {
            var client = new HttpClient();

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(URL + "rent"),
                Headers = {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer "+ token },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" }
                },
            };

            List<Rent> rents = new List<Rent>();

            try
            {
                using (var response = client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            rents = javaScriptSerializer.Deserialize<List<Rent>>(result);
                        }

                        success = true;
                        information = "";

                        return rents;
                    }
                    else
                    {
                        success = false;

                        if (response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            information = "Sesja została zakończona, aby kontynuować proszę się zalogować jeszcze raz";
                        }
                        else
                        {
                            information = "Błąd przy łączeniu się z API";
                        }
                    }

                    return rents;
                }
            }
            catch
            {
                success = false;
                information = "Błąd przy łączeniu się z API";
                return rents;
            }
        }
    }
}
