﻿using Biblioteka.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.ViewModel
{
    public class CheckBookOutListViewModel
    {
        public List<Rent> Rents { get; set; }
        public string Error { get; set; }
    }
}
