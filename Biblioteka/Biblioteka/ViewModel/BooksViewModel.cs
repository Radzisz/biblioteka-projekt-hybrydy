﻿using Biblioteka.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.ViewModel
{
    public class BooksViewModel
    {
        public List<Book> Books { get; set; }
        public string Result { get; set; }
        public string TypeResult { get; set; }
        public List<string> Type { get; set; }
        public string Error { get; set; }
    }
}
