﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.ViewModel
{
    public class ShowBookViewModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public int publishYear { get; set; }
        public int numberOfAllBooks { get; set; }
        public int numberOfAvailableBooks { get; set; }
    }
}
