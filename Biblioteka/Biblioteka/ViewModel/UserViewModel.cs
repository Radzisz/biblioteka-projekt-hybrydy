﻿using Biblioteka.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Biblioteka.ViewModel
{
    public class UserViewModel
    {
        public List<User> Users { get; set; }
        public string Result { get; set; }
        public string Error { get; set; }
    }
}
